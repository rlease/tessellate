from PIL import Image
import random
import math

def generate_voronoi(img):
	width, height = img.size
	v_img = Image.new('RGB', (width, height))
	# TODO: Hard coded for now, probably a way to find a good amount of cells in relation to the 
	# width and height
	cell_count = 300
	centers = []
	center_colors = []

	# Generate random points to form polygons around
	# TODO: Use a poisson disc sampling rather than a random sample
	for i in range(cell_count):
		x = random.randrange(width)
		y = random.randrange(height)
		r, g, b = img.getpixel((x, y))
		centers.append({'x': x, 'y': y})
		center_colors.append({'r': r, 'g': g, 'b': b})

	# TODO: This is going to be horrendously slow by brute forcing. As cell_count gets up to a usable size
	# this is going to take forever. Consider bowyer-watson or the sweep-line algorithm
	for y in range(height):
		for x in range(width):
			min_dist = math.hypot(width - 1, height - 1)
			j = -1

			for i in range(cell_count):
				dist = math.hypot(centers[i]['x'] - x, centers[i]['y'] - y)
				if dist < min_dist:
					min_dist = dist
					j = i
				v_img.putpixel((x, y), (center_colors[j]['r'], center_colors[j]['g'], center_colors[j]['b']))

	v_img.save('output/test.png')

	return v_img

img_path = 'images/dog_small.jpg'
img = Image.open(img_path)
v_img = generate_voronoi(img.convert('RGB'))
img.show()
v_img.show()
